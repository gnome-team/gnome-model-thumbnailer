# Gnome Model Thumbnailer

This is a thumbnailer for the GNOME desktop that will generate previews
of 3D models in your file manager (e.g GNOME Files)

Currently it previews .obj, .md2, .md3, and .ms3d but it can easily be extended
to produce previews for all formats supported by [assimp](https://github.com/assimp/assimp)

## Installation

You'll need meson, ninja, and the required dependencies:

```
$ mkdir builddir
$ cd builddir
$ meson ..
$ ninja
$ ninja install
```

## License

This project is released under the MIT license.

